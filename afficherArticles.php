<?php

$dsn = 'mysql:host=ne66219-001.eu.clouddb.ovh.net;port=35567;dbname=projetgit';
$user = 'projetgit';
$password = 'Gerald34';

if (array_key_exists('supprimer', $_POST)) {
    $idSupprimer = $_POST['id-suppression'];

    try {
        $dbh = new PDO($dsn, $user, $password);

        // 4 : Insertion dans la base de données
        $sql = "DELETE FROM `ingredient`
        WHERE id= '$idSupprimer' ";

        $stmt = $dbh->prepare($sql);

        // envoie le SQL en base de données
        // renvoie true si ok en base
        // renvoie false si erreur
        $success = $stmt->execute();
    } catch (Exception $e) {
        print_r($e);
    }
}

$contacts = [];

$dbh = new PDO($dsn, $user, $password);

try {

    $statement = $dbh->prepare("SELECT  id,nom_ingredient, poid_ingredient, prix_ingredient,nomFichier_ingredient FROM projetgit.ingredient ");
    $statement->execute();

    $contacts = $statement->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-Manger</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js"
        integrity="sha512-BNaRQnYJYiPSqHHDb58B0yaPfCu+Wgds8Gp/gU33kqBtgNS4tSPHuGibyoeqMV/TJlSKda6FXzoEyYGjTe+vXA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <link rel="icon" type="image/png" href="image/portrait.jpg">
    <meta name="viewport" content="width=device-width" />
</head>
<body>

<div class="container flex flex-wrap justify-between items-center mx-auto">
  <h2>Mes données</h2>

<?php if (empty($contacts) === false): ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">id </th>
            <th scope="col">nom </th>
            <th scope="col">Poid</th>
            <th scope="col">Prix</th>
            <th scope="col">Imga</th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($contacts as $contact):

?>

            <tr>
                <th scope="row"><?php echo $contact['id']; ?></th>
                <td><?php echo htmlspecialchars($contact['nom_ingredient']); ?></td>
                <td><?php echo htmlspecialchars($contact['poid_ingredient']); ?></td>
                <td><?php echo htmlspecialchars($contact['prix_ingredient']); ?></td>
                <td>
                <img width="100vh"  src="image/<?php echo $contact['nomFichier_ingredient'] ?>"> </img>
                </td>
                <td>

                <form action="" method="post">

                <input type="hidden" name="id-suppression" value="<?php echo $contact['id']; ?>">
                <br></br>

                <input type="submit" value="Supprimer" name="supprimer">

                </form>
                </td>


            </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        <?php else: ?>
            <p>
                Aucune donnée enregistrée.
            </p>
        <?php endif;?>
        <?php ?>
        </div>
</body>
</html>
