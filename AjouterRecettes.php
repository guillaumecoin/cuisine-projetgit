<?php

error_reporting(-1);
ini_set('error_reporting', E_ALL);

?>

    <!DOCTYPE html>
    <html lang="fr">

    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-Manger</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js"
        integrity="sha512-BNaRQnYJYiPSqHHDb58B0yaPfCu+Wgds8Gp/gU33kqBtgNS4tSPHuGibyoeqMV/TJlSKda6FXzoEyYGjTe+vXA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <link rel="icon" type="image/png" href="image/portrait.jpg">
    <meta name="viewport" content="width=device-width" />
    </head>
<body>
<?php include "navbar.php"?>

<br><br><br>
<center>
  <div class="container flex flex-wrap justify-between items-center mx-auto -mx-3 mb-6">
    <div class="w-full max-w-sm md:w-1/2 px-3 mb-6 md:mb-0">
        <!-- début form NE PAS TOUCHER -->
    <h4>Formulaire de création d'article :</h4>
        <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" action="" method="post" enctype="multipart/form-data">
            <div class="mb-4">

            <label class="block text-gray-700 text-sm font-bold mb-2" for="nom">Nom de l'article</label>
                <input class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" name="nom_recette" placeholder="Entrez le nom de la recette">
                <br>

                </div>
            <label class="block text-gray-700 text-sm font-bold mb-2" for="nom">Description</label>
                <input class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" id="largeur" name="description_recette" placeholder="Entrez la description">


                <label for="file"> Sélectionner l'image</label>
                <input class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" type="file" name="image_recette"   data-buttonText="Ajouter une image">
                <br></br>
                <input class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit" name="ajouter">
                <br></br>
        </form>
    </div>
</div></div></div>
</center>
</body>
</html>
<?php
$articles = ['nom_recette', 'description_recette'];

foreach ($articles as $article) {
    if (!array_key_exists($article, $_POST)) {
        echo "Veuillez remplir une valeur ";
        exit();
    }
}

$CHECKING_ERRORS = [
    // clef = name HTML, valeur phrase affichée à l'internaute

    'nom_recette' => "nom de la recette",
    'description_recette' => "description de la recette",

    

//    'newsletter' => 'Newsletter' // pourquoi ce champ est commenté ?
];

// 1 : On vérifie que l'utilisateur a bien entré tous les champs
foreach ($CHECKING_ERRORS as $key => $value) {
    /**
     * Si ma clef (eg: name, firstname), elle existe pas dans le tableau $_POST
     * OU ||
     * Est vide $_POST[clef] (eg: $_POST['name'])
     */
    if (
        !array_key_exists($key, $_POST)
        ||
        empty($_POST[$key])
    ) {
        echo '<div class="alert alert-danger" role="alert">Vous devez remplir le champ : ' . $value . '</div>';
        exit;
    }
}

// 2 : On récupère les données du formulaire

if (array_key_exists('ajouter', $_POST)) {

    $nom_recette = $_POST['nom_recette'];

    $description_recette = $_POST['description_recette'];

    $image_recette = $_FILES['image_recette']['name'];

}

if (!empty($_FILES['image_recette']['name'])) {
    require 'fonction.php';

    $error = $_FILES['image_recette']['name'];
    $nomImage = $_FILES['image_recette']['name'];
    $imageTmp_name = $_FILES['image_recette']['tmp_name'];

    if (ajouterimage($nomImage, $imageTmp_name, $error) === 1) {

        echo 'image copiée';
        $dsn = 'mysql:host=ne66219-001.eu.clouddb.ovh.net;port=35567;dbname=projetgit';
        $user = 'projetgit';
        $password = 'Gerald34';
        $success = false;

        try {
            $dbh = new PDO($dsn, $user, $password);


            # Chemin vers fichier texte
            $file = './recette/'.$nom_recette.'.txt';

            
            # Ouverture en mode écriture
            $fileopen =fopen("$file",'w');
          
            # Ecriture de "Début du fichier" dansle fichier texte
            file_put_contents($file,$description_recette);
            # On ferme le fichier proprement
            fclose($fileopen);   
            
            
            
            // 4 : Insertion dans la base de données
            $sql = "INSERT INTO `recette`
            ( nom_recette,description_recette, image_recette, nomFichier_recette) VALUE (
         '$nom_recette', '$description_recette','$nomImage', '$file')";

            $stmt = $dbh->prepare($sql);

            // envoie le SQL en base de données
            // renvoie true si ok en base
            // renvoie false si erreur
            $success = $stmt->execute();
        } catch (Exception $e) {
            print_r($e);
        }

// 5 : Afficher succès ou non suivant l'envoi du formulaire en base de données
        if ($success): ?>
             <div class="alert alert-primary" role="alert">
                 Les données sont envoyées a la base de données
             </div>
         <?php else: ?>
             <div class="alert alert-danger" role="alert">
                 Les données ne sont pas envoyées a la base de données
             </div>
             <?php
print_r($stmt->errorInfo());
        ?>


         <?php endif;

    } elseif ((ajouterimage($nomImage, $imageTmp_name) === 0)) {
        echo 'problème avec la photo';
    } else {
        echo "dada";
    }

}
